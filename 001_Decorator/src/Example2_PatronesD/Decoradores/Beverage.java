package Example2_PatronesD.Decoradores;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//1
public abstract class Beverage {
    //La bebida es una clase abstracta con los dos métodos. getDescription () y cost ().
    String description = "Unknown Beverage";
    Soy soyCost;
    Mocha mochaCost;
    Whip whipCost;
    

    //getDescription ya está implementado para nosotros, pero necesitamos implementar cost () en las subclases.
    public String getDescription() {
        return description;
    }
    public abstract double cost();
    
}
