package Example2_PatronesD.Decoradores;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * 2
 * @author Default
 */
public abstract class CondimentDecorator extends Beverage {
    //También vamos a exigir que todos los decoradores de condimentos implementen el método getDescription (). Nuevamente, veremos por qué en un segundo ...
    public abstract String getDescription();
    
}
