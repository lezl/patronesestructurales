/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Example2_PatronesD.JavaIO_Decoradores;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author Default
 */
//Primero, extienda FilterInputStream, el decorador abstracto para todos los InputStreams.
public class LowerCaseInputStream extends FilterInputStream {
    public LowerCaseInputStream(InputStream in) {
        super(in);
    }
    //Ahora necesitamos implementar dos métodos de lectura. Toman un byte (o una matriz de bytes) y
    //convierten cada byte (que representa un carácter) en minúsculas si es un carácter en mayúscula.
    public int read() throws IOException {
        int c = super.read();
        return (c == -1 ? c : Character.toLowerCase((char)c));
    }

    public int read(byte[] b, int offset, int len) throws IOException {
        int result = super.read(b, offset, len);
        for (int i = offset; i < offset+result; i++) {
        b[i] = (byte)Character.toLowerCase((char)b[i]);
    }
    return result;
    }
}