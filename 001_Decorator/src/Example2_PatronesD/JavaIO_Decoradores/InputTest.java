/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Example2_PatronesD.JavaIO_Decoradores;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 *
 * @author Default
 */
public class InputTest {
    public static void main(String[] args) throws IOException {
        int c;
        /*Configurar el FileInputStream
        y decorarlo, primero con
        un BufferedInputStream
        y luego nuestro nuevo
        Filtro LowerCase InputStream*/
        try {
            InputStream in =
            new LowerCaseInputStream(
            new BufferedInputStream(
            new FileInputStream("C:\\Users\\Default.DESKTOP-76MJQJJ\\Documents\\LuisZepedaGonzalez\\Laboral\\[1]-Cursos\\[1]-Java-8\\[1]-Practicas\\Starbuzz_Decorador\\src\\JavaIO_Decoradores\\prb.txt")));
            while((c = in.read()) >= 0) {
                System.out.print((char)c);
            }
            in.close();
        } catch (IOException e) {
        e.printStackTrace();
        }
    }
}