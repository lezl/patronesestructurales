/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Example3_Productos.Seguros;

import Example3_Productos.Productos.Producto;

/**
 * 4:
 * Clase: Decorador abstracto.
 * Agregar nuevas funcionalidades al objeto existente.
 */
public abstract class SeguroProducto extends Producto {
    /**
     * Relacion de asociacion con Producto
     */
    protected Producto producto;
}
