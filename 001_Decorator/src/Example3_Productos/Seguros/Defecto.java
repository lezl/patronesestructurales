/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Example3_Productos.Seguros;

import Example3_Productos.Productos.Producto;

/**
 * 6.
 * Clase que contiene la definición de un complemento al 
 * producto, contiene un seguro por defectos.
 */
public class Defecto extends SeguroProducto {

    /**
     * Constructor que recibe un producto a decorar, agregar
     * nueva funcionalidad.
     * @param producto Datos del producto.
     */
    public Defecto(Producto producto){
        this.producto = producto;
    }

    /**
     * Se sobrescribe el precio final del producto,
     * se agrega el costo del seguro por defectos del 
     * dispositivo.
     * @return Precio final del producto más costo 
     * del seguro por defectos.
     */
    @Override
    public double precio() {
        return (producto.precio() + 0);
    }

    /**
     * Se sobrescribe la especificacion final del producto,
     * se agrega a la descripción el tipo de seguro que se 
     * agrego.
     * @return La especificación del producto con un seguro.
     */
    @Override
    public String especificacion() {
        return producto.especificacion() + "Seguro por defectos.\n";
    }
    
}
