/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Example3_Productos.Productos;

/**
 * 3:
 * Clase Telefono, hereda de Producto sus métodos.
 */
public class Telefono extends Producto {

    /**
     * Método que sobrescribe al principal de la clase Producto.
     * @return El precio del producto 'Telefono'
     */
    @Override
    public double precio() {
        return 5000;
    }

    /**
     * Método que sobrescribe al principal de la clase Producto.
     * @return La descripcion y/o especificación del producto.
     */
    @Override
    public String especificacion() {
        return "Telefono X, N°2, 4RAM, 64GB";
    }
    
}
