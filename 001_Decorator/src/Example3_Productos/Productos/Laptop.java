/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Example3_Productos.Productos;

/**
 * 2:
 * Clase Laptop, hereda de Producto sus métodos.
 */
public class Laptop extends Producto{

    /**
     * Método que sobrescribe al principal de la clase Producto.
     * @return El precio del producto 'Laptop'
     */
    @Override
    public double precio() {
        return 10000;
    }

    /**
     * Método que sobrescribe al principal de la clase Producto.
     * @return La descripcion y/o especificación del producto.
     */
    @Override
    public String especificacion() {
        return "Laptop DELL, N°1, 16RAM, 500DD, ¡5 \n";
    }
    
}
