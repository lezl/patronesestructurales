/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Example3_Productos.Productos;

/**
 * 1:
 * Clase abstracta con los métodos que deben de implmentarse en cada uno
 * de los diferentes productos.
 */
public abstract class Producto {
    public abstract double precio();
    public abstract String especificacion();
}
