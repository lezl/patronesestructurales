/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Example3_Productos.Main;

import Example3_Productos.Seguros.Robo;
import Example3_Productos.Seguros.Defecto;
import Example3_Productos.Productos.Producto;
import Example3_Productos.Productos.Laptop;
import Example3_Productos.Seguros.SeguroProducto;

/**
 * Main
 */
public class Main {
    public static void main(String[] args) {
        Producto producto = new Laptop();
        producto = new Defecto(producto);
        producto = new Robo(producto);
        System.out.println("Especificaciones: \n" + producto.especificacion() + "Total: " + producto.precio());
    }
}
