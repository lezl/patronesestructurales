/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Example1_Ficheros;

/**
 *
 * @author Default
 */

public class Demo {
    public static void main(String[] args) {
        String salaryRecords = "Name,Salary\nJohn Smith,100000\nSteven Jobs,912000";
        DataSourceDecorator encoded =   new CompressionDecorator(
                                            new EncryptionDecorator(
                                                new FileDataSource("C:\\Users\\Default.DESKTOP-76MJQJJ\\Documents\\LuisZepedaGonzalez\\Prb.txt")
                                            )
                                        );
        encoded.writeData(salaryRecords);
        DataSource plain = new FileDataSource("C:\\Users\\Default.DESKTOP-76MJQJJ\\Documents\\LuisZepedaGonzalez\\Prb.txt");

        System.out.println("- Input ----------------");
        System.out.println(salaryRecords);
        System.out.println("- Encoded --------------");
        System.out.println(plain.readData());
        System.out.println("- Decoded --------------");
        System.out.println(encoded.readData());
    }
}